package com.android.tokopediauiclone.data

import com.android.tokopediauiclone.R

object Menu {
    var listMenuTop= arrayListOf<MenuTop>()
    var listMenuBottom= arrayListOf<MenuTop>()
    var listBannerMenu= arrayListOf<Int>()
    var listVideo= arrayListOf<VideoItem>()
    var listDiscount= arrayListOf<DiscountItem>()

    fun addAll(){
        addDataMenuTop()
        addDataMenuBottom()
        addBannerMenu()
        addVideo()
        addDiscount()
    }

    fun addVideo(){
        listVideo.apply {
            add(VideoItem(0,"2,5 rb", "Kupon spesial", "Keep calm with uchi", R.drawable.live1, "Uciha Store"))
            add(VideoItem(0,"2,5 rb", "Kupon spesial", "Keep calm with uchi", R.drawable.live2, "Uciha Store"))
            add(VideoItem(0,"2,5 rb", "Kupon spesial", "Keep calm with uchi", R.drawable.live3, "Uciha Store"))
            add(VideoItem(0,"2,5 rb", "Kupon spesial", "Keep calm with uchi", R.drawable.live4, "Uciha Store"))
        }
    }

    fun addDiscount(){
        listDiscount.apply {
            add(DiscountItem(0,"2,5 rb", R.drawable.product1, "Rp70.0000", "Rp57.0000","20%", "Uciha Store"))
            add(DiscountItem(0,"2,5 rb", R.drawable.product2, "Rp70.0000", "Rp57.0000","20%", "Uciha Store"))
            add(DiscountItem(0,"2,5 rb", R.drawable.product3, "Rp70.0000", "Rp57.0000","20%", "Uciha Store"))
            add(DiscountItem(0,"2,5 rb", R.drawable.product4, "Rp70.0000", "Rp57.0000","20%", "Uciha Store"))
        }
    }

    fun addDataMenuTop(){
        listMenuTop.apply {
            add(MenuTop(0, "Official Store", R.drawable.official_store))
            add(MenuTop(1, "Kebutuhan Harian", R.drawable.asset_view_all))
            add(MenuTop(2, "Handphone dan Tablet", R.drawable.asset_daily_need))
            add(MenuTop(3, "Top up dan Tagihan", R.drawable.asset_h_t))
            add(MenuTop(4, "Keuangan", R.drawable.asset_moneyery))
        }
    }

    fun addDataMenuBottom(){
        listMenuBottom.apply {
            add(MenuTop(0, "Travel", R.drawable.asset_travel))
            add(MenuTop(1, "Bangga Lokasl", R.drawable.bangga_lokal))
            add(MenuTop(2, "Bazar Hari Ini", R.drawable.asset_bazzar))
            add(MenuTop(3, "Live Shopping", R.drawable.asset_moneyery))
            add(MenuTop(4, "Tokopedia Seru", R.drawable.asset_party))
        }
    }

    fun addBannerMenu(){
        listBannerMenu.apply {
            add(R.drawable.coursel_first)
            add(R.drawable.coursel_second)
            add(R.drawable.coursel_first)
            add(R.drawable.coursel_second)
            add(R.drawable.coursel_first)
        }
    }
}

data class MenuTop(
    val id: Int,
    val name: String,
    val icon: Int,
)

data class VideoItem(
    var id: Int,
    var viewer: String,
    var discount: String,
    var title: String,
    var image: Int,
    var seller: String,
)

data class DiscountItem(
    var id: Int,
    var address : String,
    var image: Int,
    var priceNormal: String,
    var priceDisc: String,
    var discount: String,
    var seller: String,
)