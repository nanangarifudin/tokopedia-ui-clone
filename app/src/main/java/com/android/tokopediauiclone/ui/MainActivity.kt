package com.android.tokopediauiclone.ui

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.tokopediauiclone.data.DiscountItem
import com.android.tokopediauiclone.data.Menu
import com.android.tokopediauiclone.data.MenuTop
import com.android.tokopediauiclone.data.VideoItem
import com.android.tokopediauiclone.databinding.*
import com.android.tokopediauiclone.showToast
import com.bumptech.glide.Glide

class MainActivity : AppCompatActivity() {
    private val binding by lazy { ActivityMainBinding.inflate(layoutInflater) }
    private var listMenuTop = arrayListOf<MenuTop>()
    private var listMenuBottom = arrayListOf<MenuTop>()
    private var listBannerMenu = arrayListOf<Int>()
    private var listVideo= arrayListOf<VideoItem>()
    private var listDiscount= arrayListOf<DiscountItem>()
    private lateinit var menuTopAdapter: MenuAdapter
    private lateinit var menuBottomAdapter: MenuAdapter
    private lateinit var bannerMenuAdapter: BannerMenuAdapter
    private lateinit var videoAdapter: VideoAdapter
    private lateinit var discountAdapter: DiscountAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        Menu.addAll()
        listMenuTop = Menu.listMenuTop
        listMenuBottom = Menu.listMenuBottom
        listBannerMenu = Menu.listBannerMenu
        listVideo = Menu.listVideo
        listDiscount = Menu.listDiscount
        menuTopAdapter = MenuAdapter(this, listMenuTop)
        menuBottomAdapter = MenuAdapter(this, listMenuBottom)
        bannerMenuAdapter = BannerMenuAdapter(this)
        discountAdapter =  DiscountAdapter(this)
        videoAdapter = VideoAdapter(this)

        binding.apply {
            menu.rvMenuTop.apply {
                adapter = menuTopAdapter
                layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            }
            menu.rvMenuBottom.apply {
                adapter = menuBottomAdapter
                layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            }
            menu.rvMenuBanner.apply {
                adapter = bannerMenuAdapter
                layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            }

            menu.video.rvVideo.apply {
                adapter = videoAdapter
                layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            }

            menu.discount.rvDiskon.apply {
                adapter = discountAdapter
                layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            }
        }

    }


    inner class MenuAdapter(
        private val context: Context,
        private val listMenu: ArrayList<MenuTop> = arrayListOf()
    ) : RecyclerView.Adapter<MenuAdapter.RecentAdapterViewHolder>() {
        inner class RecentAdapterViewHolder(val view: ItemMenuBinding) :
            RecyclerView.ViewHolder(view.root)

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecentAdapterViewHolder {
            val binding = ItemMenuBinding.inflate(LayoutInflater.from(context), parent, false)
            return RecentAdapterViewHolder(binding)
        }

        override fun onBindViewHolder(holder: RecentAdapterViewHolder, position: Int) {
            holder.view.apply {
                val data = listMenu[holder.adapterPosition]
                txtMenu.text = data.name
                Glide.with(context)
                    .load(data.icon)
                    .into(imgMenu)

                root.setOnClickListener {
                    when(data.id){
                        0,1,3,4 -> showToast(data.name)
                        5 -> showToast(data.name)
                        7 -> showToast(data.name)
                    }
                }
            }
        }

        override fun getItemCount(): Int = listMenu.size
    }

    inner class BannerMenuAdapter(
        private val context: Context,
    ) : RecyclerView.Adapter<BannerMenuAdapter.RecentAdapterViewHolder>() {
        inner class RecentAdapterViewHolder(val view: ItemBannerMenuBinding) :
            RecyclerView.ViewHolder(view.root)

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecentAdapterViewHolder {
            val binding = ItemBannerMenuBinding.inflate(LayoutInflater.from(context), parent, false)
            return RecentAdapterViewHolder(binding)
        }

        override fun onBindViewHolder(holder: RecentAdapterViewHolder, position: Int) {
            holder.view.apply {
                val data = listBannerMenu[holder.adapterPosition]
                Glide.with(context)
                    .load(data)
                    .into(imgBanner)
            }
        }
        override fun getItemCount(): Int = listBannerMenu.size

    }

    inner class VideoAdapter(
        private val context: Context,
    ) : RecyclerView.Adapter<VideoAdapter.RecentAdapterViewHolder>() {
        inner class RecentAdapterViewHolder(val view: ItemVideoBinding) :
            RecyclerView.ViewHolder(view.root)

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecentAdapterViewHolder {
            val binding = ItemVideoBinding.inflate(LayoutInflater.from(context), parent, false)
            return RecentAdapterViewHolder(binding)
        }

        override fun onBindViewHolder(holder: RecentAdapterViewHolder, position: Int) {
            holder.view.apply {
                val data = listVideo[holder.adapterPosition]
                Glide.with(context)
                    .load(data.image)
                    .into(imgMenu)
                txtViewer.text = data.viewer
                txtKupon.text = data.discount
                txtTitle.text = data.title
                txtSeller.text = data.seller
            }
        }
        override fun getItemCount(): Int = listVideo.size

    }

    inner class DiscountAdapter(
        private val context: Context,
    ) : RecyclerView.Adapter<DiscountAdapter.RecentAdapterViewHolder>() {
        inner class RecentAdapterViewHolder(val view: ItemDisccountBinding) :
            RecyclerView.ViewHolder(view.root)

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecentAdapterViewHolder {
            val binding = ItemDisccountBinding.inflate(LayoutInflater.from(context), parent, false)
            return RecentAdapterViewHolder(binding)
        }

        override fun onBindViewHolder(holder: RecentAdapterViewHolder, position: Int) {
            holder.view.apply {
                val data = listDiscount[holder.adapterPosition]
                Glide.with(context)
                    .load(data.image)
                    .into(imageView2)
                txtPrice.text = data.priceDisc
                txtPriceNormal.text = data.priceNormal
                txtAddress.text = data.address
                txtDisc.text = data.discount
            }
        }
        override fun getItemCount(): Int = listDiscount.size

    }
}